var view = {
    displayMessage: function(msg) {
        var messageArea = document.getElementById('messageArea');
        messageArea.innerHTML = msg; 
    },

    displayMiss: function(location) {
        var cell = document.getElementById(location);
        cell.setAttribute('class', 'miss');
    },

    
    displayHit: function(location) {
        var cell = document.getElementById(location);
        cell.setAttribute('class', 'hit');
    }

};

var model = {
    
    boardSize: 7, 
    numShips: 3, 
    shipsSunk: 0, 
    shipLength: 3, 

    
    ships: [
            { locations: [0, 0, 0], hits: ['', '', ''] },
            { locations: [0, 0, 0], hits: ['', '', ''] },
            { locations: [0, 0, 0], hits: ['', '', ''] }
    ],
   
    generateShipLocations: function() {
        var locations; 
        for (var i=0; i<this.numShips; i++) {
            do {
                locations = this.generateShip(); 
            } while (this.collision(locations)); 

            this.ships[i].locations = locations;
        }
        console.log("Ships array: ");
        console.log(this.ships);
    },

    
    generateShip: function() {:
        var direction = Math.floor(Math.random() *2);
        var row, col;

        if (direction === 1) {
            row = Math.floor(Math.random() * this.boardSize);
            col = Math.floor(Math.random() * (this.boardSize - this.shipLength + 1));
        } else {
            row = Math.floor(Math.random() * (this.boardSize - this.shipLength +1));
            col = Math.floor(Math.random() * this.boardSize);
        }

        var newShipLocations = []; 
        for (var i=0; i<this.shipLength; i++) {
            if (direction === 1) { 
                newShipLocations.push(row + '' + (col+i)); 
            } else {
                newShipLocations.push((row+i) + '' + col);
            }
        }
        return newShipLocations;
    },

    collision: function(locations) { 
        for (var i=0; i<this.numShips; i++) {
            var ship = model.ships[i];
            for (var j=0; j<locations.length; j++) {
                if (ship.locations.indexOf(locations[j]) >= 0) { 
                }
            }
        }
        return false; 
    },


    
    fire: function(guess) {
        for (var i=0; i<this.numShips; i++) {
            var ship = this.ships[i];
            var index = ship.locations.indexOf(guess); 
            if (index >= 0) { 
                ship.hits[index] = 'hit';
                console.log('HIT!'); 
                view.displayHit(guess); 
                view.displayMessage('You hit my ship!'); 

                if (this.isSunk(ship)) {
                    view.displayMessage('You sank my battleship!!!');
                    this.shipsSunk++; 
                }

                return true;
            } else { 
                console.log('MISSED!');
                view.displayMiss(guess); 
                view.displayMessage('Nope, you missed!'); 

                return false;
            }
        }
    },

    isSunk: function(ship) {
        for (var i=0; i<this.numShips; i++) {
            if (ship.hits[i] !== 'hit') { 
                return false;  
            }
        }
        return true; 
    } 
};


var controller = {
    guesses: 0,
    processGuess: function(guess) {
        var location = parseGuess(guess); 
        if (location) {
            this.guesses++; 
            var hit = model.fire(location); 
            
            if (hit && model.shipsSunk === model.numShips) {
                view.displayMessage('You won! All my battleships are sunk in ' + this.guesses + ' guesses!');
            }
        }
    }
};


function parseGuess(guess) {
    var alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
    if (guess === null || guess.length !== 2) {
        alert('Please enter a VALID guess: a letter and a number on the board!');
    } else {
        firstChar = guess.charAt(0); 
        var row = alphabet.indexOf(firstChar); 
        var column = guess.charAt(1); 

        if (isNaN(row) || isNaN(column)) {
            alert("Oops! That's NOT on the board!");
        } else if ( row<0 || row>= model.boardSize || column <0 || column >= model.boardSize ) {
            alert("Ooops! That's OFF the board!");
        } else {
            return row + column; 
        }
    }
    return null; 
}

function init() {
    var fireButton = document.getElementById('fireButton');
    fireButton.onclick = handleFireButton;

    var guessInput = document.getElementById('guessInput'); 
    guessInput.onkeypress = handleKeyPress; 

    model.generateShipLocations(); 
}


function handleFireButton() {
    var guessInput = document.getElementById('guessInput');
    var guess = guessInput.value.toUpperCase(); 
    controller.processGuess(guess);
    guessInput.value = ''; 
}

function handleKeyPress(e) { 
    var fireButton = document.getElementById('fireButton');
    if (e.keyCode === 13) {
      
        fireButton.click(); 
        
        return false;
    }
}

window.onload = init;
