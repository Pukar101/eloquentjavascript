$(document).ready(function() {
    $('#quizon').on('click', function() {
		$(this).hide();
		$('#quiz').show('slow');	
});
});

var quiz= document.getElementById("quiz");
var ques= document.getElementById("question");
var opt1= document.getElementById("option1");
var opt2= document.getElementById("option2");
var res= document.getElementById("result");
var nextbutton= document.getElementById("next");

var tques=questions.length;
var score=0;
var quesindex=0;

function give_ques(quesindex) {
	ques.textContent=quesindex+1+". "+questions[quesindex][0];
	opt1.textContent=questions[quesindex][1];
	opt2.textContent=questions[quesindex][2];
	return; 
};

function showProgress() {
	var currentQuestionNumber = quesindex + 1;
	document.getElementById("progress").innerHTML = "Question " + currentQuestionNumber + " of 10";	
};

give_ques(0);
function nextques()
{
	var selected_ans= document.querySelector('input[type=radio]:checked');
	if(!selected_ans)
		{alert("SELECT AN OPTION");
		return;
	}

	if(selected_ans.value==questions[quesindex][3])
		{score=score+1;}
	selected_ans.checked=false;
	     quesindex++;
	     if(quesindex==tques-1)
	     	nextbutton.textContent="Finish";
	     var f=score;
	     if(quesindex==tques)
	     {

    	quiz.style.display='none';
        result.style.display='';
        result.textContent="Your Score: "+(f) + " out of 10";
            return;
	     }
        give_ques(quesindex);
} 

function showAns () {
	document.getElementById("ans").innerHTML = questions[quesindex][3]
}