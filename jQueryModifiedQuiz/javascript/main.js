$(document).ready(function() {
    $('#quizon').on('click', function() {
		$(this).hide();
		$('#quiz').show('slow');
});
});

var quiz= document.getElementById("quiz");
var ques= document.getElementById("question");
var opt1= document.getElementById("option1");
var opt2= document.getElementById("option2");
var res= document.getElementById("result");
var nextButton= document.getElementById("next");
var totalQuestion=questions.length;
var score=0;
var questionIndex=0;


function giveQuestion(questionIndex) {
	ques.textContent=questionIndex+1+". "+questions[questionIndex][0];
	opt1.textContent=questions[questionIndex][1];
	opt2.textContent=questions[questionIndex][2];
	return; 
};
	
giveQuestion(0);
function nextQuestion() {
	var selectedAnswer= document.querySelector('input[type=radio]:checked');
	if(!selectedAnswer)
		{alert("SELECT AN OPTION");
		return;
	}

	if(selectedAnswer.value==questions[questionIndex][3])
		{score=score+1;}
	selectedAnswer.checked=false;
	     questionIndex++;
	     if(questionIndex==totalQuestion-1)
	     	nextButton.textContent="Finish";
	     var f=score;
	     if(questionIndex==totalQuestion)
	     {

    	quiz.style.display='none';
        result.style.display='';
        result.textContent="Your Score: "+(f) + " out of 10";
            return;
	     }
        giveQuestion(questionIndex);
} 

function showAnswer() {
	alert("Correct answer is option " + questions[questionIndex][3]);
}

function showProgress() {
	alert("Question " + questionIndex + " of " + totalQuestion );
}


