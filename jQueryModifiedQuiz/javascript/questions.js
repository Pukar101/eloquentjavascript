var questions = [
    ["Hasta la vista, baby (Terminator 2: Judgement Day)", "Arnold Schwarzenegger", "Sylvester Stallone", "1"],
    ["Say hello to my little friend! (Scarface)", "Robert De Niro", "Al Pacino", "2"],
    ["Alright, alright, alright, alright (Dazed and Confused)", "Matthew Perry", "Matthew McConaughey", "2"],
    ["What's in the box? (Se7en)", "Brad Pitt", "Edward Norton", "1"],
    ["Greatest trick the devil ever pulled was convincing the world he didnt exist (The Usual Suspects)", "Anthony Rapp", "Kevin Spacey", "2"],
    ["Which would be worse - to live as a monster, or to die as a good man? (Inception)", "Leonardo Di Caprio", "Michael Fassbender", "1"],
    ["Frankly my dear, I don't give a damn (Gone With The Wind)", "Clark Gable", "Bella Lugosi", "1"],
    ["I'm going to make him an offer he can't refuse (The Godfather)", "Marlon Brando", "Tommy Lee Jones", "1"],
    ["Mama always said life was like a box of choclates. You never know what you're gonna get (Forrest Gump)", "Chris Evans", "Tom Hanks", "2"],
    ["Here's Johnny! (The Shining)", "Jack Nicholson", "Michael Keaton", "1"]
    ]; 