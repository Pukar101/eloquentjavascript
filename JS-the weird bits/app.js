function b() {
    console.log(myVar);
}

function a() {
    var myVar = 2;
    b();
}

var myVar = 1;
a();



var a = 'Hello World!';

function b() {
    console.log('Called b!');
}

b();
console.log(a);



b();
console.log(a);

var a = 'Hello World';

function b()
    console.log('Called b!');
}


function b() {
    console.log('Called b!');
}
b();
console.log(a);
var a = 'Hello World!';
function b() {
    console.log('Called b!')
}



const robot = {
    _model: '1E78V2',
    _energyLevel: 100,
    get energylevel() {
        if (typeof this._energyLevel === 'number') {
            return 'My current energy level is' +
            this._energyLevel
        } else {
            return "System malfunction: cannot retirieve energy level"
        }
    }
}

console.log(robot.energylevel);


const person = {
    _age: 37,
    set age(newAge) {
        if (typeof newAge === 'number'){
            this._age = newAge;
        } else {
            console.log('You must assign a number to age');
        }
    }
};

person.age = 40;
console.log(person._age);
person.age = '40';

person._age = 'forty-five'
console.log(person._age);

const robot = {
    _model: '1E78V2',
    _energyLevel: 100,
    _numOfSensors: 15,
    get _numOfSensors() {
        if(typeof this._numOfSensors === 'number'){
            return this._numOfSensors;
        } else {
            return "Sensors are currently down."
        }
    },
    set _numOfSensors(num){
        if (typeof num === 'number' && num >= 0){
            this._numOfSensors = num;
        } else {
            console.log('Pass in a number that is greater than or equal to 0')
        }
    }
};

robot.numOfSensors = 100;
console.log(robot.numOfSensors);

const monsterFactory = (name, age, energySource, catchPhrase) => {
    return {
        name: name,
        age: age,
        energySource: energySource,
        scare() {
            console.log(catchPhrase);
        }
    }
};
const ghost = monsterFactory('Ghouly', 251, 'ectoplasm', 'BOO!');
ghost.scare();

function robotFactory(model, mobile){
    return {
        model,
        mobile,
        beep() {
            console.log('Beep Boop');
        }
    }
}

const newRobot = robotFactory('T-1000', false)
console.log(newRobot.model)
console.log(newRobot.mobile)


const robot = {
    model: '1E78V2',
    energyLevel: 100,
    functionality: {
        beep() {
            console.log('Beep Boop');
        },
        fireLaser() {
            console.log('Pew Pew');
        },
    }
};